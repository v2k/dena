﻿using UnityEngine;
using System.Collections;

/*
 * Handle the creation and placing of enemies
 */
public class EnemySpawner : MonoBehaviour
{
	public GameObject[] Enemies;
	public float SpawnFrequencyInSeconds = 0f;
    public Transform SpawnArea = null;

    private float mSpawnTimer;
	public Transform UnitParent = null;

	void Start()
    {
        if (SpawnArea == null)
        {
            Debug.LogError("Spawn Area not set.", this);
        }

        mSpawnTimer = 0;
	}
	
	void Update() 
    {
        mSpawnTimer -= Time.deltaTime;

        if (mSpawnTimer < 0f)
        {
            SpawnUnit();
            mSpawnTimer = SpawnFrequencyInSeconds;
        }
	}

    // assume uniform distribution of enemies
    void SpawnUnit()
    {
        // pick enemy
        int n = Enemies.Length;

        int index = Random.Range(0, n);

        // pick spawn point
        float x = SpawnArea.position.x + Random.Range(-SpawnArea.localScale.x * 0.5f, SpawnArea.localScale.x * 0.5f);
        float z = SpawnArea.position.z + Random.Range(-SpawnArea.localScale.y * 0.5f, SpawnArea.localScale.y * 0.5f);

        Debug.Log("(x, z) = (" + x + ", " + z + ") - " + index + "/" + n, this);
        GameObject obj = (GameObject)Instantiate(Enemies[index], new Vector3(x, 0f, z), Quaternion.identity);
        obj.transform.parent = UnitParent;
    }
}
