﻿using UnityEngine;
using System.Collections;

/*
 * FXMover: LERP an FX object from start to end; used to handle turret fire and explosions
 */ 
public class FXMover : MonoBehaviour 
{
    public GameObject FX = null;
    public GameObject ExplosionFX = null;
    public Transform StartMarker = null;
    public Transform EndMarker = null;
    public float Speed = 1.0f;

    private float mStartTime;
    private float mLength;    

    void Start() 
    {
        mStartTime = Time.time;
        mLength = Vector3.Distance(StartMarker.position, EndMarker.position);
    }

    void Update() 
    {
        // lerp from start to end dep on time of travel at this speed
        float dist = (Time.time - mStartTime) * Speed;
        float t = dist / mLength;

        // blow up and destroy our self on impact
        if (EndMarker == null)
        {
            Destroy(gameObject);
        }
        else if (t > 0.97f)
        {
            Instantiate(ExplosionFX, transform.position, Quaternion.identity);

            // deal damage to target
            float damage = 0;
            AiTurret turret = StartMarker.GetComponent<AiTurret>();
            if (turret != null)
            {
                damage = turret.Damage;
            }
            
            AiUnit unit = EndMarker.GetComponent<AiUnit>();
            if (unit != null)
            {
                unit.TakeDamage(damage);
            }

            Destroy(gameObject);
        }
        else
        {
            transform.position = Vector3.Lerp(StartMarker.position, EndMarker.position, t);
        }
    }
}
