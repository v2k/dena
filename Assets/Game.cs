﻿using UnityEngine;
using System.Collections;

/*
 * Game: Generic game update class used for handling user input
 */ 
public class Game : MonoBehaviour
{
    public Camera MainCamera = null;
    public GameObject TurretPrefab = null;
	
	void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Vector3 currentMouseClickWorldSpace = MainCamera.ScreenToWorldPoint(Input.mousePosition);
            GameObject obj = (GameObject)Instantiate(TurretPrefab);
            // shift the prefab up so that it's above the ground
            obj.transform.position = new Vector3(currentMouseClickWorldSpace.x, 6f, currentMouseClickWorldSpace.z);
        }
	}
}
