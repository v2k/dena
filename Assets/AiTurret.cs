﻿using UnityEngine;
using System.Collections;

/*
 * AiTurret: handles the firing logic of the turrets
 */ 
public class AiTurret : MonoBehaviour
{
    public float CoolDownInSeconds = 0f;
    public float Damage = 0f;
    public float Range = 0f;

    public GameObject ShotFX = null;

    private Transform UnitsList = null;
    private float CoolDownTimer = 0;

	void Start()
    {
    	CoolDownTimer = CoolDownInSeconds;
        UnitsList = GameObject.FindWithTag("Units").transform;
	}
	
	void Update()
    {
        CoolDownTimer -= Time.deltaTime;

        if (CoolDownTimer < 0f)
        {
            foreach (Transform child in UnitsList)
            {
                float dist = Vector3.Distance(child.position, gameObject.transform.position);

                if (dist < Range)
                {
                    AiUnit unit = child.GetComponent<AiUnit>();
                    if (unit != null && unit.Energy > 0f)
                    {
                        // fire!
                        GameObject obj = (GameObject)Instantiate(ShotFX);
                        obj.transform.position = gameObject.transform.position;
                        FXMover fx = obj.GetComponent<FXMover>();

                        if (fx != null)
                        {
                            fx.StartMarker = gameObject.transform;
                            fx.EndMarker = child;
                        }
                    
                        CoolDownTimer = CoolDownInSeconds;
                        break; // only fire at first valid target
                    }
                }
            }
        }
	}
}
