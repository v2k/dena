﻿using UnityEngine;
using System.Collections;

/*
 * AiUnitSlider: the enemy 'slider'
 */ 
public class AiUnitSlider : AiUnit
{
    protected override void UpdatePosition()
    {
        Vector3 pos = gameObject.transform.position;

        gameObject.transform.position = new Vector3(pos.x + Speed, 0.0f, pos.z);
    }
}
