﻿using UnityEngine;
using System.Collections;

/*
 * AiUnit: Abstract base class to handle all Ai Units in the game
 */ 
abstract public class AiUnit : MonoBehaviour
{
	public float Energy = 0f;
	public float Speed = 0f;
    public GameObject EnergyBar = null;

    private float DeathTimer = 3.0f;
    private float MaxEnergy = 0f;

    protected virtual void Start()
    {
        MaxEnergy = Energy;
    }

	protected virtual void Update()
    {
        if (Energy > 0f)
        {
            UpdatePosition();
        }
        else
        {
            DeathTimer -= Time.deltaTime;
            if (DeathTimer < 0f)
            {
                Destroy(gameObject);
            }
        }

        // hack to clean up old minions
        if (gameObject.transform.position.x > 1700f)
        {
            Destroy(gameObject);
        }
	}

    abstract protected void UpdatePosition();

    public void TakeDamage(float damage)
    {
        Energy -= damage;

        if (Energy <= 0f)
        {
            Animation anim = gameObject.GetComponentInChildren<Animation>();
            anim.wrapMode = WrapMode.ClampForever;
            anim.Play("die", PlayMode.StopAll);
            Energy = 0f;
        }

        EnergyBar.transform.localScale = new Vector3(Energy / MaxEnergy * 30.0f, 1f, 1f);
    }
}
