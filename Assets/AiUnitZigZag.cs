﻿using UnityEngine;
using System.Collections;

/*
 * AiUnitSlider: the enemy 'ZigZag'
 */ 
public class AiUnitZigZag : AiUnit
{ 
    public float ZigZagFrequencyInSeconds = 1.0f;

    private float mZigZagTimer;
    private float mDirection = 1;

    protected override void Start()
    {
        base.Start();
        mZigZagTimer = ZigZagFrequencyInSeconds;
    }

    protected override void Update()
    {
        base.Update();

        mZigZagTimer -= Time.deltaTime;

        if (mZigZagTimer < 0f)
        {
            mDirection *= -1;
            mZigZagTimer = ZigZagFrequencyInSeconds;
        }
    }

    protected override void UpdatePosition()
    {
        Vector3 pos = gameObject.transform.position;

        gameObject.transform.position = new Vector3(pos.x + Speed, 0.0f, pos.z + Speed * mDirection);
    }
}
